*Oghma here. As this story continued, it became darker and more character driven than I had originally intended. Rest assured that there is more to come, and the story will continue after this. In any case, thanks for stopping by and I hope to see you all for the next chapter.*

**Disclaimer: Unrealistic sizes ahead. All characters depicted in sexual situations within are of 18 years or older.**

"Ever felt away with me. Just once that all I need. Entwined in finding you one day..." The melodic crooning echoed through Himawari's ears, yet her mind was so far away it might as well not be there at all. It spoke a lot about her mood that even classic Nightwish couldn't lift her spirits up. In fact, if she'd turned her empathic eye inward, she probably would have noticed that she had been picking out songs full of morose and longing.

She'd been up all night without a wink of sleep, her brain just wouldn't let her. An internal debate had been raging through her mind all this time, all over her feelings for Matthew. Why did this confuse her so? Her mother's words had hit her pretty hard, and perhaps she had been chasing this unachievable perfect fantasy of love. But surely she'd still be more certain about it right? Yeah, he was the first boy she ever wanted to bear children for, but wasn't that just a Breeder instinct? Pick the fittest (or rather biggest) male to father her offspring? Though putting it like that made Matthew out to be some anonymous drone, just a source of DNA for her lineage, and that couldn't be farther from the truth! Himawari enjoyed spending time with him, not just having sex or playing games but also just those quiet moments where they just talked or spent time enjoying each other's company. But of course she would want to get along well with the father of her kids, that was obvious. It wasn't uncommon for the fathers of her siblings to become great friends with the family, and Yukiko was all too happy to "share" them with her more grown-up offspring. So why didn't she want her mother to breed with-

"Oh Goddess..." Himawari said out loud. After all that debate it had finally hit her. Even though she had come to the conclusion over time, she still found herself surprised by the revelation. "I...love him!" it felt so right to finally say it. "I love him!!" she said again to herself, this time out of joy rather than surprise. While there was still that tiny twinge of doubt left within her, Himawari now felt willing to suppress it, willing to take that risk and just go for it. Without a second thought she bolted up to her feet and ran over to her phone, ignoring the nausea and fatigue of going a night without sleep. She could rest afterwards, right now she just had to tell him. Swiftly, she scrolled through her contacts, dug up Matthew's name and hit the call button. The dial tone rang for a moment or two before finally stopping.

"Hello?" came the voice on the other line. Right away Himawari could tell it wasn't Matthew.

"Oh um...is Matthew there?" she asked

http://bigasspics.me/pin/pictureual-poses-for-pictures-repost-sophiabody-and-fat-women-pics

"No he's not. This is his father." Himawari's blood froze, she really hadn't been prepared for this. She had to quickly think of a lie.

"Oh! Well, my name's Himawari. Matthew has been talking to me about Jesus and I've just been saved!" Hopefully, she was using the right terminology. "I wanted to talk to him and thank him personally for leading me to Christ. Can you tell him I called?" There was a long pause on the other end.

"Young lady." He replied flatly. "That is an awful thing to lie about." Once again, Himawari felt herself freeze up in terror. "Let me be blunt with you, I know who you are and why you're calling my son."

"But-"

"Don't try to call this number. I can only hope you one day come to your senses and find the lord. But unless that day comes, you will never see or hear from Matthew ever again." With that, he hung up the phone unceremoniously. Himawari just collapsed to the floor. Oh Goddess, what had she done?

*

June was scooping the food neatly into the Tupperware container, making sure that not a single splatter went anywhere. It had stayed in the fridge overnight, long enough for the cheese sauce to congeal into a semisolid. With any luck, it would keep well enough to last him the trip. When it was filled to the brim, she grabbed the lid and sealed it off. She was in the process of putting it in a bag when she heard her husband's voice.

"What's that?" he asked flatly.

"His favorite; Buffalo Chicken Mac & Cheese." She responded softly. "It's a long drive, I thought he would like to have it on the way. A little taste of home while he's-"

"A little taste of home?" he exclaimed in mild, somewhat dismissive exasperation. "June, he's not going to camp or off to college. This is a rehabilitation retreat for the good of his soul! We have apparently been coddling him too much, he needs tough love more than comfort food right now."

"I-I know." She stammered. "But he's going to be going through a lot, and I wanted to make sure he knows we still love him."

"He will June, he will." Said the pastor as he reached out and touched her shoulder. "When he has been cured, he will understand why we did this. The temporary sufferings on Earth are nothing compared to what he would experience in the pits of Hell, what we do we do for his sake. Do you understand?" his tone seemed soft and soothing towards June, the way it usually was when he explained things to her.

"Yes dear." She replied, taking the Tupperware over to the fridge. There was no point in arguing with him, somehow he always managed to get his way.

*

"Where are you going?" Ruth asked softly, the poor thing was on the verge of crying. It hurt Matthew so much to see her like this. But as much as he wanted to say no to his father, he knew deep down that it would do him no good; Although it wasn't outright said, Matthew knew that he wouldn't be able to stay in the house if he refused to go. Many times over his father had endorsed the exiling and disownment of children if they strayed too far from the narrow path to heaven. And if he was tossed out, where would he go? By the time he reached any of his friend's houses, his father would have gotten in touch and encouraged them to shun him. He wouldn't be the first in his community to suffer this fate. Maybe Himawari? No... no he couldn't see her now; it would just break his heart all over again. How could he impose on her when he knew she didn't feel the same way about him? It suddenly occurred to him that he'd been spacing out a little too long. He had to give Ruth an answer soon.

"Don't worry Ruth." He knelt down, putting on a feigned smile. "I'm just going away to connect with the lord better. I won't be gone long."

"Really?"

"Yes, I promise." He reassured, reaching out and ruffling her hair the way he always did when he wanted to calm her down. It was kind of funny, in some ways he felt more like Ruth's father than her older brother. He was always the one who comforted Ruth when she was sad, kept her company and played with her when she was bored. "I shouldn't be gone more than a couple weeks or so."

"Okay Matthew..." she said, reaching over to hug him goodbye. Sadly, he had to break off pretty quickly. With a sigh, he grabbed the handle of his wheel suitcase and exited his room, dragging it behind him. He quickly made his way through the house, and out the front door where his father was waiting for him.

The pastor had pulled the car out of the garage and onto the curb. His engine was still running and the trunk was wide open. It was a safe neighborhood, so there was no worry there. He had opted to wait in the walkway for his son, his face had shifted to one of attempted comfort, that same smile he gave when speaking for the congregation. "Did you get everything?"

"Yeah... not much to pack really." Matthew answered as he picked up his bag, patting it slightly to show that it was full.

"That's good." Pastor Greg said, helping his son put the bag in the trunk. When that was done, he placed his arm on his son's shoulder comfortingly. Despite everything, he still carried empathy for Matthew. "You're acting like you're off to the gallows!" he said with a slight chuckle.

"I-I kind of feel like I am Dad."

"Look, I understand; It's got to be scary to suddenly pack up and leave like this. It's certainly going to be unusual living in a new place, but don't worry! You're just going for treatment; you're not going off to die you're going off to get better."

"Yeah...yeah I guess so." Matthew mumbled.

"This is just a trial for the good of your soul, remember what it says in James: -Blessed is the man that endureth temptation: for-"

"when he is tried, he shall receive the crown of life, which the Lord hath promised to them that love him.'" Matthew finished for him. The same passage he quoted weeks ago when he thought Matthew was just talking about a friend.

"Very good." Said Greg. "Now, let's get a move on. It's a long drive and we don't want to be late."

"Yes father." Matthew replied. With that, he quietly entered the car and buckled up. They wasted no more time in driving off. Matthew looked back at his house one last time, just watching it get smaller and smaller as they got further away.

*

"Himawari"? Came a voice from the hallway, snapping her out of her trance. In all the drama that had been unfolding, she had completely forgotten that Akari had come to visit. She was the eldest daughter in Yukiko's brood, and currently the only one fully Japanese by lineage. The spitting image of her mother, Akari was a fairly tall and voluptuous woman of 22. Her long black hair was neatly styled in a hime cut, displaying her deep brown eyes and full pink lips wonderfully. Of course, attention was rarely on her face, but rather just a few inches below it. Breeders were known for being especially busty, it was after all an advantage for feeding their offspring, but Akari was particularly gifted there even by breeder standards; Big and round, they easily dwarfed her head in terms of size. Himawari had never asked about her cup size but just at a glance they couldn't be any smaller than a LL. Puberty and 4 pregnancies had definitely been kind to her. Though a little narrower than Himawari's, her hips still flared out beautifully, supporting a proportionately huge, round derriere. At the moment she wasn't especially dressed up, just a plain blue t-shirt and low riding jeans, with only the bare minimum of makeup needed to bring her natural beauty out.

"Akari!" she exclaimed, struggling to her feet and rushing over to give her big sister a hug. "Sorry! I completely forgot you were coming!"

"Oh don't worry about it!" Akari said with a laugh as she returned the hug. "From what mom told me, you've got a lot on your mind right now." Himawari's gaze sunk to the ground as she broke off from her sister.

"Oh..." she muttered, kind of embarrassed. "Haha told you about that didn't she?"

"Well, just the basics." Akari replied. "I mean she's pretty eager to be a grandmother again, and I got a basic rundown on why you're so upset right now. But she only told as much as she needed to." How considerate of Yukiko. "You don't have to say what's wrong Hima, not if you don't want to."

"No it's...it's fine." Himawari said, raising her head. After taking a deep breath, she explained everything that had happened, from the day she'd met Matthew to the phone call she just had. All the while, Akari listened intently and asked only a handful of questions. When she had finished, Akari raised her hand to her chin in a gesture of ponderance.

"So what do you think happened to him?"

"Honestly? I don't know." Himawari admitted. "His dad just told me not to call. He's probably screening his phone calls or something now."

"What do you want to do?"

"I...I want to tell him how I feel." She confessed. "Even if he rejects me now, I want to tell him. He needs to know that I..."

"Love him?"

"...Yes. But I don't know where he lives! I tried to reach him on Steam and Discord but his accounts are already deleted!" Akari nodded solemnly in response. "What should I do Akari?!" Himawari asked, her eyes welling up a little with tears.

"Calm down, don't worry!" Akari reassured her, doing her best to keep Himawari from bursting into tears. "Look... do you know where he goes to School?"

"Y-yeah... over at uh... what was it... Tilton High!" Himawari exclaimed when it finally came to her.

"Perfect! All we gotta do is just show up there on Monday and ask for him!" Akari said, clapping her hands together in determination.

"Y-yeah!" Himawari said in a wave of relief. "A-and he told me he's on the track team! So we know where to look!"

"There you go!" Akari reassured with a warm smile. The tension was rapidly lifting, that small little taste of hope was having a visible effect on Himawari. "I um..." continued Akari. "I was going to go out today, I had plans with a couple of studs." It was pretty obvious to Himawari what Akari was alluding to. Though she could probably handle both of them herself, she'd probably set this up expecting to take Himawari out with her. "But if you like, I can cancel them if you need me around- "

"No, no it's fine." Himawari responded, waving her right hand a bit. "I think I'd rather be alone for a little while. I just... I don't think I'm up for sex right now, you know?"

"Of course." Akari replied. "Are you going to be okay?"

"Eventually I guess." She sighed. "But you go ahead. Have fun." Akari nodded, turned around and started to walk away. "Oh... Akari?"

"Yes?" she didn't have time to turn around before she felt Himawari's hands wrap around her in a deep hug.

"Thank you."

*

"We're here." Said Pastor Greg unceremoniously. Matthew just slowly turned his head to stare blankly at the location. The complex was big and elaborate; A single large red brick building faced the parking lot, peppered with windows and the large double sliding glass doors. Surrounding the building was a high fence, with thick pointed bars spaced together just enough to see through, obviously to keep their tenants from escaping. Through the fence, Matthew could see a set of additional buildings, with balconies and sliding doors, many of them had clothes drying on racks. Inbetween the buildings was a large grassy field, marked with chalk at set locations. Some kind of sports field maybe?

Once they had parked, they wasted no time in gathering everything up. Straight to business as usual, it seemed his father had no intention of dragging it out. As soon as the bags were collected, the two of them made their way towards the entrance, where two figures stood in wait for them, a man and a woman.

"Welcome!" said the man of the duo. He looked to be in his mid to late 30's, while not elderly looking he definitely held a few lines of age and had a bit of a "dad bod". His deep brown hair was short and curly, just long enough to have presence but not enough to look messy. While shaven, he had a slight degree of stubble, which gave his face a bit of a darker shade to it. It was hard to tell just what color his eyes were, they were some sort of halfway point between green and brown. Hazel perhaps? "You must be Matthew." He continued, looking Matthew's way and extending his hand. "My name's Cal. It's nice to meet you." The way he spoke was... a little off. The words were nice and welcoming, but the tone carried with it a hint of forcefulness and intimidation behind it. Somehow, Matthew knew then and there he was going to be afraid of this man. He quickly returned the handshake, certain that it would be worse for him if he didn't.

"Nice to meet you." He replied softly. Matthew took the opportunity to take a look at his companion. She was about the same age as her companion, though age had been a bit kinder to her than it had been to him. Her long blonde hair was tied back in a neat ponytail, with her darker roots showing off. Light eyeshadow and plain lipstick adorned her face, just enough to make it look like she was wearing none at all. As for her physique; she seemed to be fairly muscular and stocky, the kind of build one would expect to see on law enforcement, not at all the softer curvier female body Matthew had suddenly become used to. Her expression was stern and sharp, helping to give her an aura of intimidation.

"Alright, if you'll step inside, we have some paperwork we'll need you to fill out." Said Cal flatly, gesturing inside.

*

"Please empty your bags and pockets and place all personal items aside from clothes and approved reading materials into these bins." Cal said as he pulled out 3 large clear plastic containers. Matthew looked back towards the hallway for a moment, from this angle he couldn't see his father anywhere. Surely, he couldn't have left already? "Matthew!" Cal's voice jolted his attention back to the supervisor. "Come on, chop chop." His tone was escalating, it wasn't quite yelling or even lecturing level yet, but something about it scared Matthew into submission. He didn't think it was possible for anyone to be more intimidating than his own father, but here he was. Quickly obeying, Matthew's hands dove into his pockets and emptied everything inside; His wallet, his house keys and a small collection of loose change. When that was done, Matthew bent over and swiftly unzipped the roller suitcase, rifling through it to show that indeed it only had clothes, save of course for one black leather-bound book. "What's that?" Cal asked.

"Just my Bible." Matthew answered, pulling it out and showing the cross inlay on the cover. The gilding had flaked away over the years, but the overall shape was still obvious.

"Which translation?"

"Why does that matter?" he queried.

"Which translation?" repeated Cal, it was clear there was to be no questioning.

"K-King James." Replied Matthew, turning the book on its side to show the side. Cal examined it thoroughly, almost as if he was inspecting it for any signs of forgery.

"Right, that's an approved translation. You can keep it." Matthew breathed a sigh of relief. "No phone?" Cal asked after taking a quick look through his things.

"No... my father took it from me when he sent me here. He said I couldn't be trusted with it..." Cal raised an eyebrow in skepticism.

"If I find out you're lying..."

"I'm not! Matthew interrupted.

"IF I find out you're lying!!" Cal's voice rose even further. Matthew visibly recoiled, despite the fact that Cal had yet to actually yell properly. "It will be much worse for you. Understand?"

"Y-yes... and I'm not." Matthew was quite shaken.

"And what are these?" Cal asked as he reached over and grabbed a pair of Matthew's shorts.

"My shorts?" he replied in confusion.

"Do you do any sports?"

"Yeah, track!"

"Well shorts are not allowed." Cal continued, placing the shorts in the container. "They're too suggestive, create too many impure thoughts. Any sports are to be played in loose sweatpants only."

"Oh..." Matthew said. "I um... only own one pair of those."

"We have laundry machines available for your use." He replied. "Right... everything seems in order." Cal continued as he closed off the bin. Matthew hadn't brought enough to even fill one up, so he simply placed that one with the rest of the tenants. After writing Matthew's name on the lid, he stacked the other two up and put them away.

"When do I get my things back?" Matthew asked.

https://bigasspics.me

"When you leave." Cal replied, shutting the conversation down quickly. "Alright, now come with me. You'll probably want to say your goodbyes before we go through orientation." With that, he lead Matthew out into the hallway, where the Pastor was thankfully still waiting for him.

"Well, I guess I'll see you when you get out son." Said Greg, once again putting on that smile of his. He held his hand out in a clear gesture for a handshake. The pastor was never one for hugs from anyone other than his wife. Matthew accepted the gesture and shook his father's hand.
"Yeah...I guess you will."

"Don't worry, I'll be praying for you every day." That was about as affectionate as he could get, but Matthew knew all too well just how serious that was. "Well, I need to get going, and I think you do too." He said as he broke off the handshake. "Goodbye Matthew."

"Goodbye dad."

*

Himawari sighed harshly. Though there wasn't anything she could really do until Monday, the anxiety about the whole thing was eating her up inside. What did she expect to happen? Would everything be resolved if she just told him how she felt? His father was already furious with her, and just saying she loved him wouldn't take back the pain he'd experienced, her earlier rejection may have shattered his previous feelings. Would he even speak to her at all? Something inside her was saying that she should just leave it be, that it was better to end this, move on and save herself any more potential grief. No, she couldn't do that, not now. One way or another she had to try and fix her mistake, no matter how painful it would be she had to do it. Still... the anxiety from waiting was absolute torture.

For a good long while Himawari just laid there on her back, staring up at the ceiling. She really should be doing something to try and take her mind off things, but her options were kind of shot at the moment. Right now, she couldn't really muster up enough enthusiasm for any video games, they reminded her a little too much of Matthew. There wasn't anything worth streaming at the moment, it would be a few more months before the new seasons showed up. Due to the pregnancy, she certainly couldn't smoke any weed right now. And while her sister meant well earlier, it would just feel...wrong to go and have sex with another man before she managed to resolve this whole ordeal. Her mind found itself drifting back to the memories of Matthew. He was such a considerate and sweet lover, yet when he got going he really became more animal than man. It was a few minutes of playing through her memories before Himawari realized that her hand had sunk into her panties and was playing with her pussy.

With another sigh Himawari pulled her hand away and sat up. She was frustrated, no question about it. At least she knew what she had to do now, and anything worth doing was worth doing right. Without a second thought, Himawari quickly helped herself out of her clothes. No sensual stripping this time, just an eager rush to get herself naked. When she was done though, she took a moment to look herself over. It wouldn't be too much longer before her belly started to swell with life, and her curves would follow suit to better prepare her for motherhood. With any luck, Matthew would be around to enjoy the changes along with her.

After a little fiddling to make sure she was all lubed up, Himawari strolled over to her dresser and opened up the bottom drawer. Inside was a large collection of dildos and vibrators, like any Breeder woman would keep. The only question was which one to use? Normally she'd make her way down a list, ramping it up in intensity as she reached her peak. But right now she wasn't in the mood for slow simulated intimacy, no she needed to feel thoroughly fucked. Well, there was only one in her collection that could properly scratch that itch. Himawari quickly dug through before she finally uncovered the one she had been looking for; the one she'd nicknamed "flagpole".

Flagpole was immense and heavy, perfectly suited for her breeder tastes. The toy was about 15 inches long with proportionate girth to match, a deep red color molded exactly to the shape of a real cock, complete with prominent veins all along the length. At the bottom was a series of suction cups, as it was the only way to properly secure something this heavy to the ground. It had been an 18th Birthday present, one Himawari had ended up enjoying more than the new car she also got. Of course, she usually needed to warm herself up to it, but after taking Matthew? Well, now it didn't seem so intimidating. Still, it wouldn't hurt to give herself a little extra help...

With a deep breath, Himawari opened her mouth wide and drew flagpole into her mouth. The toy slid inside with relative ease as she relaxed her jaw and throat. It lacked the taste, heat and texture of a real cock, so it wasn't really scratching her BJ queen itch. But that wasn't really the point of this anyways. She quickly bobbed her head up and down, slathering it with as much saliva as she could manage. After a few minutes of this, she popped off, noticing that she had left behind a reflective sheen. Good, that was probably going to be enough.

Turning it over, Himawari licked each of the suction cups and then lowered it to the floor. With a hard push, she stuck it to the ground and released her grip. Flagpole wobbled for a moment, but still held in place. Himawari then stood above the facsimile phallus, one leg over each side, and began to lower herself down to the ground.

It was always a little tricky to aim properly like this, as such the tip of flagpole brushed against Himawari's labia before moving past. It felt a little nice, but it wasn't what she wanted. Raising herself a little bit, Himawari gripped the toy and held it still. Gently, she sank back down and slowly speared herself on it. She let out a soft gasp as the hard plastic spread her pussy and made its way inside, her nerves inside tingling with delight. It was a softer, gentler kind of stimulation than what she was now used to, which surprised her a great deal. Not 3 weeks ago, it was a bit of a struggle to fit the toy inside her, but now she was able to "open up" with relative ease. To Himawari's disappointment, there wasn't that small twinge of pain she got with a good stretching.

"Matthew..." she whispered, thinking back to him as she sunk even lower, getting a good 8-9 inches in her. Closing her eyes, she did her best to visualize him before bouncing her hips up and down rapidly, sinking more and more into her pussy on every stroke. If anyone was watching, they'd be treated to quite the spectacle no matter which side they viewed her from. Her monstrous melons and tremendous tush jiggled erotically with her movements, creating a rhythmic clapping sound alongside the wet squelching of her nethers. Himawari moaned a little, feeling the stirrings of a small orgasm building within her. "Matthew." She moaned again as the warmth flowed through her body. "Ah!" she gasped when it finally struck her, her pussy quivering a little bit. It wasn't enough to overwhelm her completely, but the joyous sensations still felt nice, a good distraction. Sadly, it receded almost as soon as it arrived. With a frustrated sigh, Himawari increased her pace. Hopefully the next one would be what she needed.

*

"Everyone, could I have your attention please?" Cal's voice rang through the room, amplified by the microphone he was speaking through. It was a pretty standard auditorium; A well-lit stage with a wooden floor and big red curtains, metal folding chairs arranged in neat rows, wide windows along the walls to allow in a lot of natural light, and speakers placed strategically at the sides of the stage. There was still plenty of daylight, so the place was brightly illuminated. About 20 or 30 people of various ages were seated near the front in the folding chairs, obviously breeders from the first glance: Well-toned and handsome men sporting rather pronounced and noticeable bulges, and lovely curvaceous women that radiated fertility. While most of them were around his age, Matthew noticed that quite a few appeared to be much older, late 20's and early 30's. "Today we have two important events. First and foremost, let me be the first to introduce Matthew." Cal continued, gesturing over to him. Matthew swallowed hard and looked out into the audience, or rather, right through them. "As I'm sure you've guessed, he's just joined us today. Let's all welcome him to the program."

"Welcome, brother Matthew." Came the chorus of voices automatically. A cornucopia of tones and emotions ran through Matthew's ears, overwhelming him in empathic information. There were hints of moroseness blended in with some enthusiasm, both feigned and genuinely attempted.

"I expect you all to treat him with platonic respect." Cal ordered sternly. "He is here to be cured just as the rest of you are." He then turned to Matthew. "Introduce yourself if you would please?" Without any resistance, Matthew slowly walked over to the microphone. It took him a good minute or so before he could muster up the courage to speak.

"H-hello... I'm Matthew Hartford, I'm from San Miguel." He stammered. "And...and I'm a breeder. I've come here to...to seek the Lord's forgiveness and cleanse myself of my sins." With that, he quietly stepped away from the microphone. Maybe he was supposed to say more, but Matthew just couldn't muster up the strength to do so. Luckily, it seemed that Cal caught on and stepped forward to the microphone.

"Thank you brother Matthew, would you please take a seat wherever you can?" Matthew nodded softly and stepped down off the stage. "Now on that note..." Cal continued. "Today is also the day of Sister Isabella's confessions. 'Belle, would you please come up to the stage?" Just as Matthew had sat down, he noticed a nearby girl just a few seats away standing up to walk over to Cal. She was a young Latina girl about his age, with long thick curly brown hair neatly tied up in a ponytail. Her skin was a bit lighter than Himawari's though still much deeper than Matthew's own, smooth in complexion and free from blemish. As one would expect from a Breeder girl, she was exceptionally buxom with full round breasts, though he couldn't tell her cup size from a glance Matthew estimated them to be about the size of volleyballs. No less impressive was her backside, tremendously wide and equally round as near as he could see. Isabella was dressed extremely conservatively, with a white long-sleeved shirt buttoned all the way up to her neck and a long blue skirt that flowed about halfway down her shins. Right now, she was wearing no makeup at all, but between her full plump lips, amber brown eyes and narrow cheekbones she still carried a visage of beauty and loveliness. In her hands was a small stack of paper, heavily wrinkled and marked with handwritten words in black ink.

"Wait...what's this about?" Matthew asked, leaning over to his neighbor, a black man who seemed to be about college age. He was a fair bit taller and more heavily muscled than Matthew, with long thin dreadlocks and a short neatly trimmed beard. While not nearly as well dressed as some of them, he was currently adorned in a plain grey shirt and blue jeans. His face was rugged and quite handsome, with sharp prominent cheekbones and dark warm eyes.

"You'll find out." He said bluntly, turning to Matthew and placing a finger on his lips. Right, of course, there was no way Cal wouldn't object to them talking right now. Luckily, he seemed not to have noticed. As Isabella climbed onto the stage, she carefully approached the microphone. Taking a deep breath, she looked down at the notes and began to speak.

"My name is Isabella Mendoza, and I have sinned before the lord." Her voice was a little high pitched, and was wavering in nervousness. "About 2 years ago I started...I started experimenting with...with my body. Then later, I took a boy to my room and we... had sex outside of holy union." Her voice just continued to crack, obvious to everyone that she was on the verge of tears. "I just couldn't stop, and over the next few months I s-slept with so many boys I lost count. I kept doing this until I..." she swallowed hard and began to cry. "Until I became pregnant. It was only then that my parents found out and I was... I was sent here. I ask the lord for forgiveness a-and hope that I can find the strength to turn from my sins." She broke down in sobs, unable to carry on. It was only after she collapsed to the ground with her face in her hands that Cal approached her again.

"And how do you feel now?" he asked. The mic barely picked up on what he was saying, but it was enough to make it audible. Isabella took her hands away from her face, her eyes were already all read and puffy. Yet she still couldn't muster the willpower to look him in the face.

"B-broken." She answered.

"That's good." Cal said softly but sternly. "That shows that deep down, you know what you did was wrong. Do you now see how your ungodly lifestyle will bring you nothing but misery?"

"Yes..." she whispered. Nobody could hear it, but they all knew what she said.

"Don't you wish now you had waited until marriage like a good Christian? Don't you wish you had a loving husband to support you? Don't you wish you had made a family rather than broken one up?" Isabella cried aloud at the last one, wailing and sobbing almost hysterically at it. Cal just stood there, staring at the weeping girl before him with cold sternness for a few moments. When it seemed that Isabella had tired herself out, only then did Cal kneel down and place his hand on her back. "Now, let us pray."

*

"Is that...is that what we do here?" Matthew asked, folding up two chairs and tucking them under his arms.

"Eventually yeah." Said the dreadlocked man, carrying a couple of his own towards the rows of folded up chairs. "There's a lot of things you have to go through first; examining your life choices, attending sermons, platonic group activities and the like... but sooner or later you're going to have to do that." He sighed, walking over to get some more chairs.

"And this is supposed to help us?" exclaimed Matthew. "That poor girl was breaking down in tears right in front of everyone!" The man paused and slowly turned his head behind him to look at Matthew.

"Be careful what you say." He warned. "'Round here, we're expected to snitch when hear something like that." Matthew froze in place, his eyes growing wide in fear. "You're lucky I don't do that." He said as he went back to his task.

"T-thank you." Matthew didn't know what else to say.

"It's fine. You're new." He replied as he collected a few more chairs. "The name's Marcus by the way."

"Nice to meet you." Said Matthew automatically, going over to help him get the last of them. "How long have you been here Marcus?"

"About a year."

"That long?" Marcus paused and gave a morose chuckle at that.

"Trust me, that aint long. Some of the folks here have been here twice as long as I have."

"H-how long are we supposed to stay?" Matthew asked softly, his voice cracking in despair.

"Until we're cured." Marcus answered flatly, placing the last two chairs with the rest. "You hungry Matt?"

"Y-yeah I guess..." he answered. "And it's Matthew, not Matt."

"Alright, if you say so." Said Marcus with a shrug. "Come on, I'll show you where the cafeteria is."


https://bigasspics.me/pin/category/instagram
